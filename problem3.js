const problem3 = (inventory) => {
  const sortModel = inventory;

  if (inventory.length) {
    for (let i = 0; i < sortModel.length; i++) {
      for (let j = i + 1; j < sortModel.length; j++) {
        if (sortModel[i].car_model.localeCompare(sortModel[j].car_model) > 0) {
          let temp = sortModel[i];
          sortModel[i] = sortModel[j];
          sortModel[j] = temp;
        }
      }
    }
    return sortModel;
  }
  return [];
};

module.exports = problem3;
