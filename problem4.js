const problem4 = (inventory) => {
  const carYears = [];
  if (inventory.length) {
    for (let i = 0; i < inventory.length; i++) {
      carYears[i] = inventory[i].car_year;
    }
    return carYears;
  }
  return [];
};

module.exports = problem4;
