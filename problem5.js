const problem5 = (year) => {
  const oldCars = [];
  for (let i = 0; i < year.length; i++) {
    if (year[i] < 2000) {
      oldCars.push(year[i]);
    }
  }
  return oldCars;
};
module.exports = problem5;
