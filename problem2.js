const problem2 = (inventory) => {
  if (inventory.length) {
    const element = inventory[inventory.length - 1];
    return "Last car is a " + element.car_make + " " + element.car_model;
  }
  return [];
};

module.exports = problem2;
