const problem6 = (inventory) => {
  const BMWAndAudi = [];

  if (inventory.length) {
    let counter = 0;
    for (let i = 0; i < inventory.length; i++) {
      if (inventory[i].car_make == "Audi" || inventory[i].car_make == "BMW") {
        BMWAndAudi[counter] = inventory[i];
        counter += 1;
      }
    }
    return BMWAndAudi;
  }
  return [];
};

module.exports = problem6;
